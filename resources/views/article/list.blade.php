@extends('layouts.app')
@section('content')
@if($articles==null)
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">No articles</div>
            </div>
            </div>
            </div>
            </div>
@else
@foreach($articles as $article)
<div class='container'>
    <div class=" col-md-offset-1">
<div class="row">
                <div class="col-md-6">
                    <a href="#">
                        <img style="width: 100%;height: 300px"class="img-responsive" src="{{asset('uploads/')}}/{{$article->image}}" alt="">
                    </a>
                </div>
                <div class="col-md-5">
                    <h3>{{$article->title}} <small>Posted By {{$article->user->name}}</small></h3>
                    <p class="glyphicon glyphicon-time">  {{$article->updated_at}}</p>
                    <p>{{$article->description}}</p>
                    <a class="btn btn-primary" href="{{route('detail',['article_id'=>$article->id,'user_id'=>$article->user->id])}}">Read more <span class="glyphicon glyphicon-chevron-right"></span></a>
                    <a class="btn btn-primary" href="{{route('editarticle',['id'=>$article->id])}}">Edit <span class="glyphicon glyphicon-edit"></span></a>
                    <a class="btn btn-danger" href="{{route('deletearticle',['id'=>$article->id])}}">Delete <span class="glyphicon glyphicon-remove"></span></a>
                </div>
            </div>
</div>
</div>
<hr>
@endforeach
@endif
@endsection

