@extends('layouts.app')
@section('content')
<div class='col-xs-8 col-xs-offset-2'>
    <table class='table table-hover'>
        <tr>
            <td>ID</td>
            <td></td>
            <td>{{trans('DefineForm.name')}}</td>
            <td>{{trans('DefineForm.email')}}</td>
            @if(Auth::user()->role==Config::get('constant.SUPERADMIN') or Auth::user()->role==Config::get('constant.ADMIN'))
            <td>Role</td>
            
            <td>Action</td>
            @endif
            <td>Detail</td>
        </tr>
     
        @if(Auth::user()->role==Config::get('constant.SUPERADMIN'))
        @foreach($all as $demo)
        <tr>
            
            <td>{{$demo->id}}</td>
            @if($demo->profile==null) <td></td>
            @else
            <td><img style="width: 30px;height: 30px " src="{{asset('uploads/')}}/{{$demo->profile->avatar}}"></td>
            @endif
            <td>{{$demo->name}}</td>
            <td>{{$demo->email}}</td>
            <td>@if($demo->role==Config::get('constant.SUPERADMIN'))Superadmin
                @elseif($demo->role==Config::get('constant.ADMIN')) Admin
                @else Member
                @endif
            </td>
            <td>
                <a href="{{route('edit',['id'=>$demo->id])}}">Edit</a>
                <a href="{{route('delete',['id'=>$demo->id])}}">Delete</a>
            </td>
               <td>
                        <a href="{{route('profile',['id'=>$demo->id])}}">{{$demo->name}}'s Profile</a>
                    </td>
        </tr>
        @endforeach
        @endif
        
        @if(Auth::user()->role==Config::get('constant.ADMIN') or Auth::user()->role==Config::get('constant.MEMBER'))
            @foreach($all as $demo)
            @if($demo->role==Config::get('constant.MEMBER'))
                <tr>
                    
                    <td>{{$demo->id}}</td>
                    @if($demo->profile==null) <td></td>
                    @else
                    <td><img style="width: 36px;height: 36px " src="{{asset('uploads/')}}/{{$demo->profile->avatar}}"></td>
                    @endif
                    <td>{{$demo->name}}</td>
                    <td>{{$demo->email}}</td>
                     @if(Auth::user()->role==Config::get('constant.ADMIN'))
                    <td>@if($demo->role==Config::get('constant.SUPERADMIN'))Superadmin
                @elseif($demo->role==Config::get('constant.ADMIN')) Admin
                @else Member
                @endif
                    </td>
                    <td>
                       
                        <a href="{{route('edit',['id'=>$demo->id])}}">Edit</a>
                        <a href="{{route('delete',['id'=>$demo->id])}}">Delete</a>
                    </td>   
                    @endif
                    <td>
                        <a href="{{route('profile',['id'=>$demo->id])}}">{{$demo->name}}'s Profile</a>
                    </td>
                    </tr>
                   
                    
            @endif
        @endforeach
        @endif
        
    </table>
</div>
@endsection

