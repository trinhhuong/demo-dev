@extends('layouts.app');
@section('content')
        <div class='col-xs-8 col-xs-offset-2'>
            <form class='form-horizontal' action="{{route('postcreate')}}" method="Post">
                <input type="hidden" name="_token" value='{{csrf_token()}}'>
                <div class='form-group {{ $errors->has('name') ? ' has-error' : '' }} '>
                    <label>{{trans('DefineForm.name')}}</label>
                    <input type='text' class='form-control' placeholder="Name" name='name' value="{{old('name')}}">
                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name')}}</strong>
                                    </span>
                                @endif
                </div>
                 <div class='form-group {{ $errors->has('email') ? ' has-error' : '' }}'>
                    <label>{{trans('DefineForm.email')}}</label>
                    <input type='text' class='form-control' placeholder="Email" name='email' value="{{old('email')}}">
                    @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                </div>
                 <div class='form-group {{ $errors->has('password') ? ' has-error' : '' }}'>
                    <label>{{trans('DefineForm.password')}}</label>
                    <input type='password' class='form-control' placeholder="Password" name='password' >
                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                </div>
                 <div class='form-group {{ $errors->has('password_confirmation') ? ' has-error' : '' }}'>
                    <label>Password Confirm</label>
                    <input type='password' class='form-control' placeholder="Password Confirm" name='password_confirmation' >
                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                </div>
                @if(Auth::user()->role==Config::get('constant.SUPERADMIN'))
                <div class='form-group'>
                    <label>Role</label>
                    <select name='role' class='form-control'>
                        <option value={{Config::get('constant.ADMIN')}}>Admin</option>
                        <option value={{Config::get('constant.MEMBER')}} >Member</option>        
                    </select>
                </div>
                @endif
                @if(Auth::user()->role==Config::get('constant.ADMIN'))
                <select  style="display: none" name='role' class='form-control'>
                        <option value={{Config::get('constant.MEMBER')}} >Member</option>        
                    </select>
                @endif
                <button type="submit" class='btn btn-default'>{{trans('DefineForm.add')}}</button>
            </form>
        </div>      
 @endsection   
