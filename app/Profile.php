<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table='profile';
    public $timestamp=false;
    protected $fillable=['user_id','avatar','dob','address','phone'];
    public static function findById($id) {
        $demo=  Profile::find($id);
        return $demo;
    }
    public static function updateById($input,$id) {
        $demo=  Profile::findById($id);
        $demo->fill($input);
        $demo->save();
    }
    public static function insert($input){
      $demo=new Profile();
      $demo->fill($input);
      $demo->save();
    }
}
