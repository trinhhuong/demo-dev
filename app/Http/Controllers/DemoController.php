<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Demo;
use App\Profile;
use App\Http\Requests\AddDemoRequest;
use App\Http\Requests\EditDemoRequest;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;
class DemoController extends Controller
{
   
    /**/
    protected function create_demo(array $data)
    {
        return [
            'name' => $data['name'],
            'role' => $data['role'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'remember_token' => bcrypt($data['password_confirmation']),
        ];
    }
    public function create(){
       return view('demo.create');
   }
   
   public function store(AddDemoRequest $request){
       $input=$this->create_demo($request->all());
      
       Demo::insert($input);
       return redirect()->route('list');
   }
  public function index() {
       $alldemo=Demo::getAll();
     
       return view('demo.list',['all'=>$alldemo]);
   }
  /* public function getcolum() {
       $demo=DB::table('demo')->select('username','sex')->get();
       print_r($demo);
       if(Schema::hasColumn('demo','username')){
           echo 'co';
       }
       if(Schema::hasTable('demo')){
           echo 'co';
       }
   }*/
   public function edit($id) {
       $demo=Demo::findById($id);
       return view('demo.edit',['demo'=>$demo]);
   }
   public function update(EditDemoRequest $request,$id) {
       //dd($request);
       //var_dump($id);die;
       $input = $request->all();
       Demo::updateById($input, $id);
       return redirect()->route('list');
   }
   public function delete($id){
       Demo::deleteById($id);
       return redirect()->route('list');
   }
  
   public function profile($id) {
       
       $demo=Demo::findById($id);
       return view('demo.profile',['demo'=>$demo]);
   }
   
  
}
