<?php

namespace App\Http\Middleware;

use Closure;

class isowner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->id==$request->route()->getParameter('id')){
        return $next($request);}
        else return redirect ()->route('list');
    }
}
